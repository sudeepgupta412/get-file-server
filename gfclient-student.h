/*
 *  This file is for use by students to define anything they wish.  It is used by the gf client implementation
 */
 #ifndef __GF_CLIENT_STUDENT_H__
 #define __GF_CLIENT_STUDENT_H__
 
 #include "gf-student.h"

int gfr_transferfile(gfcrequest_t *gfr);

ssize_t gfs_sendcontents(gfcrequest_t *gfr, void *data, size_t len);
 #endif // __GF_CLIENT_STUDENT_H__