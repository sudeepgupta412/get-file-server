#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <string.h>
#include <netdb.h>
#include <netinet/in.h>

#include "gfclient.h"
#include "gfclient-student.h"


#define RETURN_ERROR -1
#define RETURN_SUCCESS 0
#define MAX_REQUESTS 100
#define HEADERSIZE 300
#define SCH_LEN 1000
#define STATUS_LEN 50
#define FILE_BUFFER 4096

//static gfcrequest_t **openRequests;
//unsigned int openrequests_no = 1;

static char *statuses[] = { "OK", "FILE_NOT_FOUND", "ERROR", "INVALID" };
static const char *methodname = "GET";
static const char *Headerdelim = "\r\n\r\n";
static const char *getscheme = "GETFILE";

void error(char *msg)
{
    perror(msg);
    exit(1);
}





struct gfcrequest_t {
    char *server;
    char *path;
    char requestHeader[HEADERSIZE];
    unsigned short port;
    void (*headerfunc)(void*, size_t, void *);
    void *headerarg;
    void (*writefunc)(void*, size_t, void *);
    void *writearg;
    gfstatus_t status;
    size_t bytesReceived;
    size_t fileLength;
    int sockfd;
};


void gfc_cleanup(gfcrequest_t *gfr){
	if (gfr != NULL)
		free(gfr);
	else
		error("client not created or already freed\n");
}

gfcrequest_t *gfc_create(){
	    struct gfcrequest_t *gfr;
	//if (openrequests_no < MAX_REQUESTS){
    gfr=(gfcrequest_t*) malloc(sizeof(struct gfcrequest_t));
	//openRequests[openrequests_no++] = gfr;
	//
	//else{
		//printf("MAX requests reached\n");
	//}
	
return gfr;
}

size_t gfc_get_bytesreceived(gfcrequest_t *gfr){
	if (gfr == NULL) 
		error ("client not created\n");
		
    return gfr->bytesReceived;
}

size_t gfc_get_filelen(gfcrequest_t *gfr){
	if (gfr == NULL)
		error ("client not created\n");
			
    return (gfr -> fileLength);
}

gfstatus_t gfc_get_status(gfcrequest_t *gfr){
		if (gfr == NULL)
			error ("client not created\n");
		
		return gfr->status;
    
}

void gfc_global_init(){
	//openRequests = malloc(MAX_REQUESTS * sizeof(gfcrequest_t *));
    //memset(openRequests, '\0', sizeof(MAX_REQUESTS * sizeof(gfcrequest_t *)));

}

void gfc_global_cleanup(){
	//int i;
	//for (i = 0; i<=openrequests_no; i++)
	//	gfc_cleanup(openRequests[i]);

	//free(openRequests);
	
}

int readheader(gfcrequest_t *gfr){

	if (gfr == NULL)
		error("client not created\n");
	char header[HEADERSIZE]="";
	char temp[HEADERSIZE] = "";
	ssize_t headerbytesReceived = 0;
	size_t headerbytes = 0;
	size_t bytesleft = 0;
	int returnvalue = 0;
	
	//printf("bytes received before readheader %zu\n", gfr->bytesReceived );
	while(1){
		if ((headerbytesReceived = read(gfr->sockfd, temp, HEADERSIZE)) < 0){
			printf("Failed to read from socket\n");
		}
		
		if (headerbytesReceived > 0){
			if (headerbytes + headerbytesReceived <= HEADERSIZE) {
				memcpy (header + headerbytes, temp, headerbytesReceived);
				bytesleft = 0;
			}
			//if (headerbytesReceived < 0)
			else if (headerbytes < HEADERSIZE){
				memcpy (header + headerbytes, temp, HEADERSIZE - headerbytes);
				//memcpy (header, temp, HEADERSIZE - headerbytes);
				bytesleft = headerbytes;
				headerbytes = HEADERSIZE;
			}
			else{
				printf("bytes received more than header size\n");
				gfr->status = GF_ERROR;
				returnvalue = -1;
				break;
			}

			char scheme[SCH_LEN] = "";
			char status[STATUS_LEN] = "";
			size_t filesize = 0;
			int filebegin = 0;
			if (sscanf(header, "%s %s %zd\r\n\r\n%n", scheme, status, &filesize, &filebegin)>EOF) {
				
				//printf("header received = %s %s %lu %d\n", scheme, status, filesize, filebegin);
				if ((strcmp(scheme, "") ==0) || (strcmp(status, "") == 0) || (strcmp(scheme,"GETFILE") != 0)){
					gfr->status = GF_INVALID;
					returnvalue = -1;
					break;
				}
				if (strcmp(status,"OK")!= 0) {
					if (strcmp(status,"FILE_NOT_FOUND")!=0){
						if (strcmp(status,"ERROR")!=0){ 
							gfr->status = GF_INVALID;
							returnvalue = -1;
							break;
						}
					}
				}
				
				
				
				if ((strcmp(scheme, "") !=0) && (strcmp(scheme,"GETFILE") == 0)){
					if ((strcmp(status, "") != 0) && (strcmp(status,"OK") == 0)){
						gfr->status = GF_OK;
						returnvalue = 0;
						//printf("readheader status is OK\n");
					}
					else if ((strcmp(status, "") != 0) && (strcmp(status,"FILE_NOT_FOUND") == 0)){
						gfr->status = GF_FILE_NOT_FOUND;
						//printf("readheader status is FILE NOT FOUND\n");
						returnvalue = -1;
						break;
					}
					else if ((strcmp(status, "") != 0) && (strcmp(status,"ERROR") == 0)){
						gfr->status = GF_ERROR;
						returnvalue = -1;
						break;
					}
					else if((strcmp(status, "") != 0) && (strcmp(status,"INVALID") == 0)){
						gfr->status = GF_INVALID;
						returnvalue = -1;
						break;
					}
					else {
						gfr->status = GF_INVALID;
						returnvalue = -1;
						break;
					}					
				
			
				gfr->fileLength = filesize;
			
				if (filebegin < headerbytesReceived){
				///if (filebegin > headerbytesReceived){	
					ssize_t bytestowrite = headerbytesReceived - filebegin;
					char *filestart = header + filebegin;
					gfr->writefunc(filestart, bytestowrite, gfr->writearg);
					gfr->bytesReceived  = gfr->bytesReceived + bytestowrite;
				}
			
				if (headerbytes == HEADERSIZE){
				//if (headerbytes < HEADERSIZE){
					char *start = temp + HEADERSIZE - bytesleft;
					gfr->writefunc(start, bytesleft, gfr->writearg);
					gfr->bytesReceived = gfr->bytesReceived + bytesleft;
				}
				returnvalue = 0;
				break;
				}
			}
			else{
					printf("Header invalid in scanf\n");
					gfr->status = GF_INVALID;
					returnvalue = -1;
					break;
				}			
			if (((strcmp(scheme, "") ==0) || (strcmp(scheme,"GETFILE") != 0)) || ((strcmp(status, "") == 0) || (strcmp(status,"OK") != 0))){
				printf("header was invalid or malformed\n");
				gfr->status = GF_INVALID;
				returnvalue = -1;
				break;
			}
		}
		else if(headerbytesReceived == 0){
			printf("Socket was closed or header malformed\n");
			gfr->status = GF_INVALID;
			returnvalue = -1;
			break;
		}
		else if (headerbytesReceived <0){
			printf("Error reading from socket\n");
			gfr->status = GF_ERROR;
			returnvalue = -1;
			break;
		}

	}
	//printf("bytes transfered after readheader %zu ", gfr->bytesReceived);			
	//printf("file length in header %zu ", gfr->fileLength);	
	//printf("returning from readheader with value %d\n", returnvalue);
	return returnvalue;
}



int gfc_perform(gfcrequest_t *gfr){
	
	if (gfr == NULL)
		error("Client not created\n");
	
	int option = 1;
	struct hostent *server_h;
	struct sockaddr_in serv_addr;
	int returnvalue = 0;
	int readheadervalue = 0;
	
	gfr->sockfd = socket(AF_INET, SOCK_STREAM, 0);
	gfr->bytesReceived = 0;
	if (gfr->sockfd <0){
		printf("ERROR opening socket");
		gfr->status = GF_ERROR;
		returnvalue = -1;
		}
	
	if(setsockopt(gfr->sockfd, SOL_SOCKET,(SO_REUSEPORT | SO_REUSEADDR),(char*)&option, sizeof(option)) < 0)
		{
		printf("setsockopt failed\n");
		returnvalue = -1;
		close(gfr->sockfd);
		}
	
	server_h = gethostbyname(gfr->server);
	unsigned long server_addr_nbo = *(unsigned long *) (server_h->h_addr_list[0]);
    if (gfr->server == NULL) {
        printf("ERROR, no such host\n");
        exit(0);
    }
    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
	serv_addr.sin_addr.s_addr = server_addr_nbo;
    serv_addr.sin_port = htons(gfr->port);
	if (connect(gfr->sockfd,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0) {
		printf("ERROR connecting\n");
		gfr->status = GF_ERROR;
		returnvalue = -1;
	}
	else{
	//else printf("Connection established\n");
	snprintf(gfr->requestHeader, sizeof(gfr->requestHeader), "%s %s %s%s", getscheme, methodname, gfr->path, Headerdelim);
	printf("header lenght = %lu\n", sizeof(gfr->requestHeader));
	
	//filepath = gfr->path;
	//printf("file path is %c \n", filepath[0]);
	
	printf("%s %s %s %s", getscheme, methodname, gfr->path, Headerdelim);
	if ((strcmp(getscheme, "") ==0) || (strcmp(methodname, "") == 0) || (strcmp(Headerdelim,"") == 0) || (strcmp(gfr->path,"") == 0)){
		gfr->status = GF_INVALID;
		returnvalue = -1;
		
	}
		if ((strcmp(getscheme,"GETFILE")!= 0) || (strcmp(methodname,"GET")!=0) || (strcmp(Headerdelim,"\r\n\r\n")!=0) ){
			gfr->status = GF_INVALID;	
			returnvalue = -1;
		}
		if (gfs_sendcontents(gfr, gfr->requestHeader, sizeof(gfr->requestHeader)) < 0) {
			printf("Failed to send header\n");
			returnvalue = -1;
		}
	
			readheadervalue = readheader(gfr);
	
	
		if (readheadervalue == 0){
			//printf("readheader status = 0\n");
			if (gfr_transferfile(gfr) == 0){
				returnvalue = 0;
			}
			else {
				returnvalue = -1;
			}
		}
		else if(readheadervalue == -1){
			//printf("read header status = -1\n");
			returnvalue = 0;
			gfr->bytesReceived = 0;
		}
	}

		if (gfr->status == GF_INVALID){
			returnvalue = -1;
			gfr->bytesReceived = 0;
		}
    return returnvalue;
}


int gfr_transferfile(gfcrequest_t *gfr){
	ssize_t filebytesreceived;
	char filecontents[FILE_BUFFER];
	
	int returnvalue = 0;
	//printf("bytes received before transferfile %zu\n", gfr->bytesReceived );
	while(gfr->bytesReceived < gfr->fileLength){
		filebytesreceived = read(gfr->sockfd, filecontents, FILE_BUFFER);
		
		if (filebytesreceived > 0){
			if (filebytesreceived + gfr->bytesReceived < gfr->fileLength){
				gfr->writefunc(filecontents, filebytesreceived, gfr->writearg);
				gfr->bytesReceived = gfr->bytesReceived + filebytesreceived;
			}
			else{
				gfr->writefunc(filecontents, gfr->fileLength - gfr->bytesReceived, gfr->writearg);
				gfr->bytesReceived = gfr->fileLength;
				gfr->status = GF_OK;
				returnvalue = 0;
				break;
			}
		}
		
		else if(filebytesreceived == 0){
			printf("Socket closed. transfer could not happen\n");
			returnvalue = -1;
			break;
		}
		else{
			printf("Error reading socket\n");
			returnvalue = -1;
			break;
		}
	}		
	//printf("bytes received after transferfile %zu\n", gfr->bytesReceived );
return returnvalue;
}


void gfc_set_headerarg(gfcrequest_t *gfr, void *headerarg){
	if ((gfr != NULL) || (headerarg != NULL))
		gfr->headerarg = headerarg;
	else
		error("gfr or header argument is NULL");
}

void gfc_set_headerfunc(gfcrequest_t *gfr, void (*headerfunc)(void*, size_t, void *)){
	if ((gfr == NULL) || (headerfunc == NULL))
		error("client not present or header function is empty\n");
	
	gfr->headerfunc = headerfunc;
}

void gfc_set_path(gfcrequest_t *gfr, char* path){
	if ((gfr != NULL) || (path != NULL))
		gfr->path = path;
	else
		error("client or Path is NULL");
}

void gfc_set_port(gfcrequest_t *gfr, unsigned short port){
	if (gfr != NULL)
		gfr->port = port;
	else
		error("client or gfr is null for set_server");
}

void gfc_set_server(gfcrequest_t *gfr, char* server){
  if ((gfr != NULL) || (server != NULL))
	  gfr->server = server;
  else
	  error("client or gfr is null for set_server");

}

void gfc_set_writearg(gfcrequest_t *gfr, void *writearg){
	if (gfr != NULL) 
		gfr->writearg = writearg;
	else
		error("gfr has not been created");
}

void gfc_set_writefunc(gfcrequest_t *gfr, void (*writefunc)(void*, size_t, void *)){
	if ((gfr == NULL) || (writefunc == NULL))
		error("client not created or nothing to write\n");
	
	gfr->writefunc = writefunc;
}

char* gfc_strstatus(gfstatus_t status){

    return statuses[status];
}


ssize_t gfs_sendcontents(gfcrequest_t *gfr, void *data, size_t len){
	size_t bytesleft = len;
	size_t total = 0;
	size_t bytesSent = 0;
	
	if (gfr == NULL)
		error("ctx and data empty to send\n");
	
	if(data == NULL){
		return bytesSent;
	}
	
	//printf("header length = %lu \n", bytesleft);
	while(total < len && bytesleft > 0){
		bytesSent = send(gfr->sockfd, data+total, bytesleft,0);
		if (bytesSent <0){
			printf("Error sending request over socket");
			break;
		}
		total = total + bytesSent;
		bytesleft = bytesleft - bytesSent;
	}
	//printf("bytes sent = %lu \n", bytesSent);
	return bytesSent;
 
}