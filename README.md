# get file server & client
Getfile is a simple protocol used to transfer a file from one computer to
another that we made up for this project.  A typical successful transfer is
illustrated below.

![Getfile Transfer Figure](illustrations/gftransfer.png)

The general form of a request is

```
<scheme> <method> <path>\r\n\r\n
```

Note:

* The scheme is always GETFILE.
* The method is always GET (though there are rumors that HEAD will be added one
	day).
* The path must always start with a ‘/’.

The general form of a response is

```
<scheme> <status> <length>\r\n\r\n<content>
```

Note:
* The scheme is always GETFILE.
* The status must be in the set {‘OK’, ‘FILE_NOT_FOUND’, ‘ERROR’, 'INVALID'}.
* INVALID is the appropriate status when the header is invalid. This includes a malformed header as well an incomplete header due to communication issues.
* FILE_NOT_FOUND is the appropriate response whenever the client has made an
	error in his request.  ERROR is reserved for when the server is responsible
	for something bad happening.
* No content may be sent if the status is FILE_NOT_FOUND or ERROR.
* When the status is OK, the length should be a number expressed in ASCII (what
	sprintf will give you). The length parameter should be omitted for statuses
	of FILE_NOT_FOUND or ERROR.
* The sequence ‘\r\n\r\n’ marks the end of the header. All remaining bytes are
	the files contents.
* The space between the \<scheme> and the \<method> and the space between the \<method> and the \<path> are required. The space between the \<path> and '\r\n\r\n' is optional.
* The space between the \<scheme> and the \<status> and the space between the \<status> and the \<length> are required. The space between the \<length> and '\r\n\r\n' is optional.

### Instructions
For Part 1 of the assignment you will implement a Getfile client library and a
Getfile server library.  The API and the descriptions of the individual
functions can be found in the gfclient.h and gfserver.h header files.  Your
task is to implement these interfaces in the gfclient.c and gfserver.c files
respectively.  To help illustrate how the API is intended to be used and to
help you test your code we have provided the other files necessary to build a
simple Getfile server and workload generating client inside of the gflib
directory.  It contains the files

* Makefile - used for compiling the project components
* content.[ch] - a library that abstracts away the task of
	fetching content         from disk.
* content.txt -  a data file for the content library
* gfclient.c - implementation of the gfclient interface.
* gfclient.h - header file for the gfclient library
* gfclient-student.h -  header file for students to modify - submitted for client only
* gfclient_download.c - the main file for the client
	workload generator.  Illustrates use of gfclient library.
* gfserver.c -  implementation of the gfserver interface.
* gfserver.h -  header file for the gfserver library.
* gfserver-student.h -  header file for students to modify - submitted for server only
* gfserver_main.c  the main file for the Getfile server.
	Illustrates the use of the gfserver library.
* gf-student.h  header file for students to modify - submitted for both client and server
* handler.c -  contains the handler callback that is
	registered with the gfserver library.
* workload.[ch] -  a library used by workload generator
* workload.txt - a data file indicating what paths should
	be requested and where the results should be stored.

### gfclient
The client-side interface documented in gfclient.h is inspired by [libcurl’s
“easy” interface](http://curl.haxx.se/libcurl/c/libcurl-easy.html).  To help
illustrate how the API is intended to be used and to help you test your code,
we have provided the file gfclient_download.c. For those not familiar with
function pointers in C and callbacks, the interface can be confusing.  The key
idea is that before the user tells the gfclient library to perform a request,
he should register one function to be called on the header
(`gfc_set_headerfunc`) and register one function to be called for every “chunk”
of the body (`gfc_set_writefunc`). That is, one call to the latter function
will be made for every `recv()` call made by the gfclient library.  (It so
happens that none of the test code actually will use the header function, but
please implement your library to support it anyway.)

The user of the gfclient library may want his callback functions to have access
to data besides the information about the request provided by the gfclient
library.  For example, the write callback may need to know the file to which it
should write the data.  Thus, the user can register an argument that should be
passed to his callback on every call.  He does this with the
`gfc_set_headerarg` and `gfc_set_writearg` functions.  (Note that the user may
have multiple requests being performed at once so a global variable is not
suitable here.)

Note that the `gfcrequest_t` is used as an [opaque
pointer](https://en.wikipedia.org/wiki/Opaque_pointer), meaning that it should
be defined within the gfclient.c file and no user code (e.g.
gfclient_download.c) should ever do anything with the object besides pass it to
functions in the gfclient library.

### gfserver
The server side interface documented in gfserver.h is inspired by python’s
built-in httpserver.  The existing code in the files handler.c and
gfserver_main.c illustrate this usage.  Again, for those not  familiar with
function pointers in C and callbacks, the interface can be confusing.  The key
idea is before starting up the server, the user registers a handler callback
with gfserver library which controls how the request will be handled.  The
handler callback should not contain any of the socket-level code.  Instead, it
should use the gfs_sendheader, gfs_send, and potentially the gfs_abort
functions provided by the gfserver library.  Naturally, the gfserver library
needs to know which request the handler is working on.  All of this information
is captured in the opaque pointer passed into the handler, which the handler
must then pass back to the gfserver library when making these calls.

