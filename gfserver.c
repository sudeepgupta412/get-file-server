#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <string.h>
#include <netdb.h>
#include <netinet/in.h>

#include "gfserver.h"
#include "gfserver-student.h"

/* 
 * Modify this file to implement the interface specified in
 * gfserver.h.
 */
struct gfcontext_t{
    size_t bytessent;
    size_t fileLength;
    int ctx_sockfd;
};


void error(char *msg)
{
    perror(msg);
    exit(1);
}

struct gfserver_t{
    int sockfd;
    int serverPort;
    int maxConnection;
    ssize_t (*handler)(struct gfcontext_t *, char *, void * );
    struct gfcontext_t *ctx;
    int status;
	void *arg;
};
 


#define BUFSIZE 4096
#define HEADERSIZE 300
#define SCH_LEN 1000
#define MET_LEN 20
#define PATH_LEN 300

static const char *getscheme = "GETFILE";
static const char *Headerdelim = "\r\n\r\n";
static const char *methodname = "GET";
static const char *ErrorResponse = "ERROR";
 
 
void gfs_abort(gfcontext_t *ctx){
	if (ctx == NULL){
		error("client not created \n");
	}
	
	if (shutdown(ctx->ctx_sockfd, SHUT_RDWR)){
		printf("Failed to shutdown client \n");
	}
	if (close(ctx->ctx_sockfd) <0 ){
		printf("Failed to close client \n");
	}
	
	free(ctx);
}

gfserver_t* gfserver_create(){
    struct gfserver_t *gfs;
    gfs=(gfserver_t*) malloc(sizeof(struct gfserver_t));
   
    return gfs;
}

ssize_t gfs_send(gfcontext_t *ctx, void *data, size_t len){
	
	if ((ctx == NULL) && (data == NULL))
		error("ctx and data empty to send\n");

	size_t bytesleft = len;
	size_t total = 0;
	size_t bytesSent = 0;

	while(total < len){
		bytesSent = send(ctx->ctx_sockfd, data+total, bytesleft, 0);
		if (bytesSent <0){
			error("Error sending request over socket");
			break;
		}
		total = total + bytesSent;
		bytesleft = bytesleft - bytesSent;
	}
	return bytesSent;
 
}



ssize_t gfs_sendheader(gfcontext_t *ctx, gfstatus_t status, size_t file_len){
	if (ctx == NULL)
		error("ctx null in gfs_sendheader");
	
	char header[50];
	
	ctx->fileLength = file_len;
	ctx->bytessent = 0;
	
	if (status == GF_OK) {
		snprintf(header, sizeof(header), "%s %s %zd %s", getscheme, "OK", file_len, Headerdelim);
	}
	else if (status == GF_FILE_NOT_FOUND){
		snprintf(header, sizeof(header), "%s %s %s", getscheme, "FILE_NOT_FOUND", Headerdelim);
	}
	else if (status == GF_INVALID){
		snprintf(header, sizeof(header), "%s %s %s", getscheme, "FILE_NOT_FOUND", Headerdelim);
	}
	else{
		snprintf(header, sizeof(header), "%s %s %s", getscheme, ErrorResponse, Headerdelim);
	}
	
	
	size_t headSize = strlen(header);
	if (gfs_send(ctx, header, headSize) < headSize){
		error("Failed to send header to client");
		return -1;
	}
	
	return headSize;
}

void gfserver_serve(gfserver_t *gfs){
	int socketfd;
	int option =1;
	struct sockaddr_in serv_addr;
	

	
	if (gfs != NULL) {
		socketfd = socket(AF_INET, SOCK_STREAM, 0);
		if (socketfd <0){
			printf("ERROR opening socket");
			gfs->status = GF_ERROR;
			//returnvalue = -1;
		}
		gfs->sockfd = socketfd;
	}
	
	
	if(setsockopt(gfs->sockfd, SOL_SOCKET,(SO_REUSEPORT | SO_REUSEADDR),(char*)&option, sizeof(option)) < 0)
		{
		printf("setsockopt failed\n");
		close(gfs->sockfd);
		exit(1);
		}
	 
	bzero((char *) &serv_addr, sizeof(serv_addr));
     serv_addr.sin_family = AF_INET;
     serv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
     serv_addr.sin_port = htons(gfs->serverPort);


	 if (bind(gfs->sockfd, (struct sockaddr *) &serv_addr,
		sizeof(serv_addr)) < 0) 
        error("ERROR on binding");	 
		
		listen(gfs->sockfd,gfs->maxConnection);
		
		while(1)
		{
			int client_fd = 0;
			struct gfcontext_t *ctx;
			struct sockaddr_in cli_addr;
			ctx=(gfcontext_t*) malloc(sizeof(struct gfcontext_t));
			socklen_t clilen = 0;
			size_t received_bytes = 0;
			ssize_t headreceived_bytes = 0;
			client_fd = accept(gfs->sockfd, 
                 (struct sockaddr *) &cli_addr, 
                 &clilen);
			if (client_fd < 0) 
				error("ERROR on accept");
	 
			ctx->ctx_sockfd =  client_fd;	
				//printf("hi 1st while\n");
			char buffer[BUFSIZE] = "";
			char header[HEADERSIZE] = "";
			
			while(1){
				received_bytes =  read(ctx->ctx_sockfd, buffer, BUFSIZE);
				
				if (received_bytes >0){
					if (headreceived_bytes + received_bytes <= HEADERSIZE){
						memcpy(header + headreceived_bytes, buffer, received_bytes);
						headreceived_bytes = headreceived_bytes + received_bytes;
					}
					else{
						memcpy(header + headreceived_bytes, buffer, HEADERSIZE - headreceived_bytes);
						headreceived_bytes = HEADERSIZE;
					}
					//printf("hi 2nd while\n");
					char scheme[SCH_LEN] = "";
					char method[MET_LEN] = "";
					char filepath[PATH_LEN] = "";
					int end = 0;
					
					//printf("hi 2nd while_2\n");
					if (sscanf(header, "%s %s %s\r\n\r\n%n", scheme, method, filepath, &end)> EOF) {
						//printf("cleared 1st if\n");
						//printf("scheme = %s\n", scheme);
						//printf("method = %s\n", method);
						//printf("filepath = %s\n", filepath);
						//printf("filepath0 = %s\n", filepath[0]);
						//printf("endposition = %d\n", end);
						//printf("headerdelim = %s", filepath);
						if ((((strcmp(scheme, getscheme) != 0) || (strcmp(method,methodname) != 0) || (filepath[0] != '/') ))) {
							//printf("header invalid = %d\n", GF_INVALID);
							gfs_sendheader(ctx, GF_INVALID, 0);
							break;
						}
							
												
						if ((scheme != NULL) && (method != NULL) && (filepath != NULL) && (end != 0)) {
							if ((strcmp(scheme, "") != 0) && (strcmp(method, "") != 0) && strcmp(filepath, "") !=0){
								if ((((strcmp(scheme, getscheme) != 0) && (strcmp(method,methodname) != 0) && (filepath[0] != '/') && (end ==0))) && (headreceived_bytes == HEADERSIZE)){ 
									gfs_sendheader(ctx, GF_FILE_NOT_FOUND, 0);
									break;
								}
								else{
									//printf("cleared if conditions\n");
									gfs->handler(ctx, filepath, gfs->arg);
									
									break;
								}
							}
						}	
					}
					printf("failed test cond\n");					
				}			
				else if(received_bytes == 0){
					error("The socket is closed. Header not proper. Header should be \"GETFILE GET <file path>\\r\\n\\r\\n\"\n");
					}
				else{
					error("Error reading data from the socket\n");
					}
		}	
						
	}
} 


void gfserver_set_handlerarg(gfserver_t *gfs, void* arg){
	if (gfs != NULL){
		gfs->arg = arg;
	}

}

void gfserver_set_handler(gfserver_t *gfs, ssize_t (*handler)(gfcontext_t *, char *, void*)){
	if(gfs != NULL){
		if(handler !=NULL)
			gfs->handler = handler;
	}

}

void gfserver_set_maxpending(gfserver_t *gfs, int max_npending){
if (gfs != NULL){
	gfs->maxConnection = max_npending;
	}
}

void gfserver_set_port(gfserver_t *gfs, unsigned short port){
if (gfs != NULL){
	gfs->serverPort = port;
}
}


